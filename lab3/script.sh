#!/bin/bash
crontab ./waked

echo ""
count=0
while read LINE
do 
	if [ "$count" -gt "1" ]
	then
		echo "$(($count-1))) $LINE"
	fi
	((count++))
done < /home/nik/Documents/eltex.-linux/lab2/waked

echo ""
echo "1-Создать, 2-Редактировать, 3-Удалить, 4-Остановить воспроизведение, 5-Выключить"
read comm

if [ "$comm" == "1" ]
then
	echo "Время срабатывания(минуты):"
	read min
	echo "Время срабатывания(час):"
	read hours
	echo "День срабатывания:"
	read day
	echo "Введите полный путь до файла с музыкой(например /home/nik/Documents/eltex.-linux/lab2/song2.mp3):"
	read song
	echo "$min $hours $day * * mplayer $song" >> ./waked
fi

if [ "$comm" == "2" ]
then
	./update.sh	
fi

if [ "$comm" == "3" ]
then
	cat /dev/null >| ./waked
	echo "SHELL=/bin/bash" >> ./waked
	echo "MAILTO=nik" >> ./waked
fi

if [ "$comm" == "5" ]
then
	exit
fi

if [ "$comm" == "4" ]
then
	killall mplayer
fi

crontab -r
crontab ./waked
./script.sh



