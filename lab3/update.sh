#!/bin/bash
echo ""
count=0
while read LINE
do 
	if [ "$count" -gt "1" ]
	then
		echo "$(($count-1))) $LINE"
	fi
	((count++))
done < /home/nik/Documents/eltex.-linux/lab2/waked

if [ "$count" -eq "2" ]
then
	echo "Будильников не найдено"
	exit
else

	echo ""
	echo "Введите номер строки, которую хотите изменить"
	read newCount

	echo "Время срабатывания(минуты):"
	read min
	echo "Время срабатывания(час):"
	read hours
	echo "День срабатывания:"
	read day
	echo "Введите полный путь до файла с музыкой(например /home/nik/Documents/eltex.-linux/lab2/song2.mp3):"
	read song

	count=0
	while read LINE
	do 
		if [ "$count" -gt "1" ]
		then
			if	[ "$(($newCount+1))" -eq "$count" ]
			then
				sed -i "y/$LINE/${min} ${hours} ${day} * * mplayer ${song}/" /home/nik/Documents/eltex.-linux/lab2/waked				
			fi
		fi
		((count++))
	done < /home/nik/Documents/eltex.-linux/lab2/waked
fi


