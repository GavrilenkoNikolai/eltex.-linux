#!/bin/bash
echo "Введите директорию: "
read dir
echo "Введите количество папок: "
read countI
echo "Введите количество подпапок: "
read countJ
echo "Введите количество файлов: "
read file
echo "Введите шаблон имени: "
read mask

for ((i=1; i<=$countI; i++))
do
  mkdir "$dir/$mask $i"
  for ((j=1; j<=$countJ; j++))
  do
    mkdir "$dir/$mask $i/$mask $j"
    for ((l=i; l<=$file; l++))
    do
      touch "$dir/$mask $i/$mask $j/$mask $l"
    done
  done
done
